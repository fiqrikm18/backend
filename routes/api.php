<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Employee\PositionController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\Absensi\IzinController;
use App\Http\Controllers\Auth\AuthController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('refresh', [AuthController::class, 'refresh'])->name('refresh');
    Route::get('user-profile', [AuthController::class, 'employeeProfile'])->name('employee_profile_test');
});

Route::group(['prefix' => 'position', 'middleware' => 'auth:api'], function () {
    // functional position routes
    Route::get('functional/', [PositionController::class, 'getAllFunctionalPosition'])->name('get_func_pos');
    Route::get('functional/{id}', [PositionController::class, 'getAllFunctionalPositionById'])->name('get_func_pos_by_id');
    Route::post('functional/create/', [PositionController::class, 'createFunctionalPosition'])->name('create_func_pos');
    Route::delete('functional/delete/{id}', [PositionController::class, 'deleteFunctionalPosition'])->name('delete_func_pos');
    Route::put('functional/update/{id}', [PositionController::class, 'updateFunctionalPosition'])->name('update_func_pos');

    // structural position routes
    Route::get('structural/', [PositionController::class, 'getAllStructuralPosition'])->name('get_struct_pos');
    Route::get('structural/{id}', [PositionController::class, 'getAllStructuralPositionById'])->name('get_struct_pos_by_id');
    Route::post('structural/create/', [PositionController::class, 'createStructuralPosition'])->name('create_struct_pos');
    Route::delete('structural/delete/{id}', [PositionController::class, 'deleteStructuralPosition'])->name('delete_struct_pos');
    Route::put('structural/update/{id}', [PositionController::class, 'updateStructuralPosition'])->name('update_struct_pos');
});

// employee routes
Route::group(['middleware' => 'auth:api', 'prefix' => 'employee'], function () {
    Route::get('/', [EmployeeController::class, 'getAllEmployeeData'])->name('get_all_employee');
    Route::get('{id}/', [EmployeeController::class, 'getAllEmployeeDataById'])->name('get_all_employee_by_id');
    Route::post('create/', [EmployeeController::class, 'createEmployeeData'])->name('create_employee');
    Route::put('update/{id}/', [EmployeeController::class, 'updateEmployeeData'])->name('update_employee');
    Route::delete('delete/{id}/', [EmployeeController::class, 'deleteEmployeeData'])->name('delete_employee');
});

// izin routes
// not tested yet
Route::group(['middleware' => 'auth:api', 'prefix' => 'izin'], function () {
    Route::get('/', [IzinController::class, 'getAllIzinData'])->name('get_all_izin');
    Route::get('nip/{nip}/', [IzinController::class, 'getAllIzinDataByNip'])->name('get_izin_nip');
    Route::get('{id}/', [IzinController::class, 'getIzinDataById'])->name('get_izin_by_id');
    Route::post('create/', [IzinController::class, 'createIzinData'])->name('create_izin');
    Route::put('update/{id}/', [IzinController::class, 'updateIzinData'])->name('update_izin');
    Route::delete('delete/{id}/', [IzinController::class, 'deleteIzinData'])->name('delete_izin');
});
