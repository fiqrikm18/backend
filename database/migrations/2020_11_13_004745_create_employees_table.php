<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->string('nip', 10);
            $table->string('name', 100);
            $table->string('email');
            $table->string('password');
            $table->unsignedBigInteger('functional_pos');
            $table->unsignedBigInteger('structural_pos');
            $table->timestamps();

            $table->foreign('functional_pos')->references('id')->on('functional_positions');
            $table->foreign('structural_pos')->references('id')->on('structural_positions');
        });
    }

    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
