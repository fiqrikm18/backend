<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSakitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sakits', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('sakit');
            $table->string('nip', 10)->references('nip')->on('employees');
            $table->timestamps();

//            $table->foreign('nip')->references('nip')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sakits');
    }
}
