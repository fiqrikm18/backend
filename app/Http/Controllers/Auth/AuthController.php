<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Employee\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->employeeService = $employeeService;
    }

    protected function createToken($token) {
        return response()->json([
            'status' => 'ok',
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
            'access_token' => $token
        ]);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->only('nip', 'password'), [
            'nip' => 'required|string|max:10',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'error' => $validator->errors()
            ]);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json([
                'status' => 'fail',
                'error' => 'Unauthorized'
            ]);
        }

        return $this->createToken($token);
    }

    public function register(Request $request) {
        $result = ['status' => 'ok'];
        $data = $request->only(
            'nip',
            'name',
            'email',
            'password',
            'password_confirmation',
            'functional_pos',
            'structural_pos'
        );

        try {
            $result['data'] = $this->employeeService->createEmployeeData($data);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function logout() {
        auth()->logout();
        return response()->json([
            'message' => 'Logout success'
        ]);
    }

    public function refresh() {
        return $this->createToken(auth()->refresh());
    }

    // for testing purpose only
    public function employeeProfile() {
        return response()->json(auth()->user());
    }
}
