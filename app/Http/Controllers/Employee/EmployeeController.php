<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Services\Employee\EmployeeService;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    protected $employeeService;

    public function __construct(EmployeeService  $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function getAllEmployeeData(Request $request) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->employeeService->getAllEmployee();
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getAllEmployeeDataById(Request $request, $id) {
        $result = ['status' => 'ok'];
        try {
            $result['data'] = $this->employeeService->getEmployeeById($id);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function createEmployeeData(Request $request) {
        $data = $request->only(
            'nip',
            'name',
            'email',
            'password',
            'password_confirmation',
            'functional_pos',
            'structural_pos'
        );
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->employeeService->createEmployeeData($data);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function updateEmployeeData(Request $request, $id) {
        $data = $request->only(
            'nip',
            'name',
            'email',
            'password',
            'password_confirmation',
            'functional_pos',
            'structural_pos'
        );
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->employeeService->updateEmployeeData($data, $id);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function deleteEmployeeData(Request $request, $id) {
        $result = ['status' => 'ok'];

        try {
            $resutl['data'] = $this->employeeService->deleteEmployeeData($id);
        } catch(Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($resutl);
    }
}
