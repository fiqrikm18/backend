<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Services\Employee\PositionService;
use Exception;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    protected $positionService;

    private $functionalType = 'functional';
    private $structuralType = 'structural';

    public function __construct(PositionService $positionService)
    {
        $this->positionService = $positionService;
    }

    public function getAllFunctionalPosition(Request $request) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->getAllPosition($this->functionalType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getAllStructuralPosition(Request $request) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->getAllPosition($this->structuralType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getAllFunctionalPositionById(Request $request, $id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->getPositionById($id, $this->functionalType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getAllStructuralPositionById(Request $request, $id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->getPositionById($id, $this->structuralType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function createFunctionalPosition(Request $request) {
        $data = $request->only('position');

        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->savePosition($data, $this->functionalType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function createStructuralPosition(Request $request) {
        $data = $request->only('position');

        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->savePosition($data, $this->structuralType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function deleteFunctionalPosition($id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->deletePosition($id, $this->functionalType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function deleteStructuralPosition($id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->deletePosition($id, $this->structuralType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function updateStructuralPosition(Request $request, $id) {
        $data = $request->only('position');
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->updatePosition($data, $id, $this->structuralType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function updateFunctionalPosition(Request $request, $id) {
        $data = $request->only('position');
        error_log($request->get('position'));
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->positionService->updatePosition($data, $id, $this->functionalType);
        } catch (Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }
}
