<?php

namespace App\Http\Controllers\Absensi;

use App\Http\Controllers\Controller;
use App\Services\Absensi\IzinService;
use Illuminate\Http\Request;

class IzinController extends Controller
{
    protected $izinService;

    public function __construct(IzinService $izinService)
    {
        $this->izinService = $izinService;
    }

    public function getAllIzinData(Request $request) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->getAllIzin();
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getIzinDataById(Request $request, $id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->getIzinById($id);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function getAllIzinDataByNip(Request $request, $nip) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->getIzinByNip($nip);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    // don't know if this work
    // just try to implement this function to router
    // then test this to check if this is ok
    public function createIzinData(Request  $request) {
        error_log($request->get('izin'));
        $data = $request->only('izin', 'nip');
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->createIzin($data);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function updateIzinData(Request $request, $id) {
        $data = $request->only('izin', 'nip');
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->updateIzin($data, $id);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function deleteIzinData(Request $request, $id) {
        $result = ['status' => 'ok'];

        try {
            $result['data'] = $this->izinService->deleteIzin($id);
        } catch (\Exception $e) {
            $result = [
                'status' => 'fail',
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }
}
