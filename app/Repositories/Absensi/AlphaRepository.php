<?php


namespace App\Repositories\Absensi;


use App\Models\Alpha;
use function GuzzleHttp\Psr7\_parse_request_uri;

class AlphaRepository
{
    // TODO: implement to service
    protected $alpha;

    public function __construct(Alpha  $alpha)
    {
        $this->alpha = $alpha;
    }

    public function create($data) {
        $alpha = new $this->alpha;
        $alpha->alpha = $data['alpha'];
        $alpha->nip = $data['nipp'];

        if ($alpha->save()) {
            return $alpha->fresh();
        }
    }

    public function delete($id) {
        $alpha = $this->alpha->find($id);
        $alpha->delete();

        return $alpha;
    }

    public function update($data, $id) {
        $alpha = $this->alpha->find($id);
        $alpha->nip = $data['nip'];
        $alpha->alpha = $data['alpha'];

        $alpha->update();
        return $alpha;
    }

    public function getAll() {
        return $this
            ->alpha
            ->with('employee')
            ->get();
    }

    public function getAlphaById($id) {
        return $this
            ->alpha
            ->with('employee')
            ->find($id);
    }

    public function getAlphaByNip($nip) {
        return $this
            ->alpha
            ->with('employee')
            ->where('nip', '=', $nip);
    }
}
