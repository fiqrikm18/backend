<?php


namespace App\Repositories\Absensi;


use App\Models\Sakit;

//!!!! TODO: implement this
//!!! TODO: documenting
class SakitRepository
{
    //!!!! TODO: implement to service
    protected $sakit;

    public function __construct(Sakit  $sakit)
    {
        $this->sakit = $sakit;
    }

    public function create($data) {
        $sakit = new $this->sakit;
        $sakit->nip = $data['nip'];
        $sakit->sakit = $data['sakit'];

        if ($sakit->save()) {
            return $sakit->fresh();
        }
    }

    public function delete($id) {
        $sakit = $this->sakit->find($id);
        $sakit->delete();
        return $sakit;
    }

    public function update($data, $id) {
        $sakit = $this->sakit->find($id);
        $sakit->nip = $data['nip'];
        $sakit->sakit = $data['sakit'];

        $sakit->update();
        return$sakit;
    }

    public function getSakitById($id) {
        return $this
            ->sakit
            ->with('employee')
            ->find($id);
    }

    public function getAllSakit() {
        return $this
            ->sakit
            ->with('employee')
            ->get();
    }

    public function getSakitByNip($nip) {
        return $this
            ->sakit
            ->with('employee')
            ->where('nip', '=', $nip);
    }
}
