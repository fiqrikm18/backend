<?php


namespace App\Repositories\Absensi;

use App\Models\Izin;

class IzinRepository {
    // TODO: check again
    protected $izin;

    public function __construct(Izin  $izin)
    {
        $this->izin = $izin;
    }

    public function getAll() {
        return $this
            ->izin
            ->with('employee')
            ->get();
    }

    public function getById($id) {
        return $this
            ->izin
            ->find($id);
    }

    /**
     * ?? don't know work or not
     */
    public function getByNip($nip) {
        return $this
            ->izin
            ->where('nip', $nip);
    }

    public function create($data) {
        $izin = new $this->izin;
        $izin->izin = $data['izin'];
        $izin->nip = $data['nip'];

        if ($izin->save()) {
            return $izin->fresh();
        }
    }

    public function update($data, $id) {
        $izin = $this->izin->find($id);
        $izin->izin = $data['izin'];
        $izin->update();

        return $izin;
    }

    public function delete($id) {
        $izin = $this->izin->find($id);
        $izin->delete();
        return $izin;
    }
}
