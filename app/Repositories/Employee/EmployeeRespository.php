<?php


namespace App\Repositories\Employee;


use App\Models\Employee;
use Illuminate\Support\Facades\Hash;

class EmployeeRespository
{
    protected $employee;

    public function __construct(Employee  $employee)
    {
        $this->employee = $employee;
    }

    /**
     * get all employee data
     * @return mixed
     */
    public function findAll() {
        return $this->employee->get();
    }

    /**
     * get employee by nip
     * @param $id
     * @return mixed
     */
    public function findById($id) {
        return $this->employee->find($id);
    }

    /**
     * insert new employee data
     * @param $data
     */
    public function save($data) {
        $employee = new $this->employee;
        $employee->nip = $data['nip'];
        $employee->name = $data['name'];
        $employee->email = $data['email'];
        $employee->password = Hash::make($data['password']);
        $employee->functional_pos = $data['functional_pos'];
        $employee->structural_pos = $data['structural_pos'];

        if ($employee->save()) {
            return $employee->fresh();
        }
    }

    /**
     * update current employee data by nip
     * @param $data
     * @param $id
     */
    public function update($data, $id) {
        $employee = $this->employee->find($id);
        $employee->nip = $data['nip'];
        $employee->name = $data['name'];
        $employee->email = $data['email'];
        $employee->functional_pos = $data['functional_pos'];
        $employee->structural_pos = $data['structural_pos'];

        $employee->update();
        return $employee;
    }

    /**
     * delete employee data
     * @param $id
     */
    public function delete($id) {
        $employee = $this->employee->find($id);
        $employee->delete();
        return $employee;
    }
}
