<?php


namespace App\Repositories\Employee;


use App\Models\FunctionalPosition;
use App\Models\StructuralPosition;

class PositionRepository
{
    protected $functionalPos;
    protected $structuralPos;

    public function __construct(FunctionalPosition $functionalPos, StructuralPosition $structuralPos)
    {
        $this->functionalPos = $functionalPos;
        $this->structuralPos = $structuralPos;
    }

    /***
     * checking passed data
     * @param $type
     * @return FunctionalPosition|StructuralPosition
     */
    private function checkType($type) {
        if ($type == 'structural') {
            return $this->structuralPos;
        } else if ($type == 'functional') {
            return $this->functionalPos;
        }
    }

    public function getAll($type) {
        return $this->checkType($type)->get();
    }

    public function getById($id, $type) {
        return $this->checkType($type)->where('id', $id)->get();
    }

    public function save($data, $type) {
        $position = $this->checkType($type);
        $position->position = $data['position'];

        if ($position->save()) {
            return $position->fresh();
        }
    }

    public function delete($id, $type) {
        $position = $this->checkType($type)->find($id);
        $position->delete();

        return $position;
    }

    public function update($data, $id, $type) {
        $position = $this->checkType($type)->find($id);

        $position->position = $data['position'];
        $position->update();

        return $position;
    }
}
