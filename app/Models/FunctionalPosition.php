<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FunctionalPosition extends Model
{
    use HasFactory;

    protected $fillable = ['position'];
}
