<?php


namespace App\Services\Absensi;

use App\Repositories\Absensi\SakitRepository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

//!!!! TODO: implement this
//!!! TODO: documenting
class SakitService
{
    protected $sakitRepository;

    /***
     * SakitService constructor.
     * @param SakitRepository $sakitRepository
     */
    public function __construct(SakitRepository $sakitRepository)
    {
        $this->sakitRepository = $sakitRepository;
    }

    public function createSakit($data) {
        $validator = Validator::make($data, [
            'nip' => 'required|string',
            'sakit' => 'required|int'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }

        $sakit = $this->sakitRepository->create($data);
        return $sakit;
    }

    public function updateSakit($data, $id) {
        $validator = Validator::make($data, [
            'nip' => 'required|string',
            'sakit' => 'required|int'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }
        DB::beginTransaction();

        try {
            $sakit = $this->sakitRepository->update($data, $id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable update data');
        }

        DB::commit();
        return $sakit;
    }

    public function deleteSakit($id) {
        DB::beginTransaction();

        try {
            $sakit = $this->sakitRepository->update($id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable delete data');
        }

        DB::commit();
        return $sakit;
    }
}
