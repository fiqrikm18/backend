<?php


namespace App\Services\Absensi;


use App\Repositories\Absensi\IzinRepository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class IzinService
{
    //!!! TODO: implement
    //!!! TODO: documenting
    protected $izinRepository;

    /**
     * IzinService constructor.
     * @param $izinRepository
     */
    public function __construct(IzinRepository $izinRepository)
    {
        $this->izinRepository = $izinRepository;
    }

    public function getAllIzin() {
        return $this
            ->izinRepository
            ->getAll();
    }

    public function getIzinById($id) {
        return $this
            ->izinRepository
            ->getById($id);
    }

    public function getIzinByNip($nip) {
        return $this
            ->izinRepository
            ->getByNip($nip);
    }

    public function createIzin($data) {
        $validator = Validator::make($data, [
            'izin' => 'required|int',
            'nip' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }

        $izin = $this->izinRepository->create($data);
        return $izin;
    }

    public function updateIzin($data, $id) {
        $validator = Validator::make($data, [
            'izin' => 'required|int',
            'nip' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }
        DB::beginTransaction();

        try {
            $izin = $this->izinRepository->update($data, $id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to update data');
        }

        DB::commit();
        return $izin;
    }

    public function deleteIzin($id) {
        DB::beginTransaction();

        try {
            $izin = $this->izinRepository->delete($id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to delete data');
        }

        DB::commit();
        return $izin;
    }
}
