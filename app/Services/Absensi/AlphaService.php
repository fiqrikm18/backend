<?php


namespace App\Services\Absensi;


use App\Repositories\Absensi\AlphaRepository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use function MongoDB\BSON\toJSON;

//!!!! TODO: Implement this
class AlphaService
{
    protected $alphaRepository;

    /***
     * AlphaService constructor.
     * @param AlphaRepository $alphaRepository
     */
    public function __construct(AlphaRepository $alphaRepository)
    {
        $this->alphaRepository = $alphaRepository;
    }

    /***
     * create alpha data in database
     * @param $data
     * @return mixed
     */
    public function createAlpha($data) {
        $validator = Validator::make($data, [
            'nip' => 'required|string',
            'alpha' => 'required|int'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()-toJSON());
        }

        $alpha = $this->alphaRepository->create($data);
        return $alpha;
    }

    /***
     * update existing alpha data in database
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateAlpha($data, $id) {
        $validator = Validator::make($data, [
            'nip' => 'required|string',
            'alpha' => 'required|int'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()-toJSON());
        }
        DB::beginTransaction();

        try {
            $alpha = $this->alphaRepository->update($data, $id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to update data');
        }

        DB::commit();
        return $alpha;
    }

    /***
     * delete existing alpha data in database
     * @param $id
     * @return mixed
     */
    public function deleteAlpha($id) {
        DB::beginTransaction();
        try {
            $alpha = $this->alphaRepository->delete($id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to delete data');
        }

        DB::commit();
        return $alpha;
    }

    /***
     * get all existing alpha data in database
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllAlpha() {
        return $this
            ->alphaRepository
            ->getAll();
    }

    /***
     * get existing alpha data by id in database
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getAlphaById($id) {
        return $this
            ->alphaRepository
            ->getAlphaById($id);
    }

    /***
     * get existing alpha data by employee nip in database
     * @param $nip
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAlphaByNip($nip) {
        return $this
            ->alphaRepository
            ->getAlphaByNip($nip);
    }
}
