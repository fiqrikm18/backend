<?php


namespace App\Services\Employee;


use App\Repositories\Employee\PositionRepository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PositionService
{
    protected $positionRepository;

    public function __construct(PositionRepository $positionRepository)
    {
        $this->positionRepository = $positionRepository;
    }

    public function getAllPosition($type) {
        return $this->positionRepository->getAll($type);
    }

    public function getPositionById($id, $type) {
        return $this->positionRepository->getById($id, $type);
    }

    public function savePosition($data, $type) {
        $validator = Validator::make($data, [
            'position' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $result = $this->positionRepository->save($data, $type);
        return $result;
    }

    public function deletePosition($id, $type) {
        DB::beginTransaction();

        try {
            $position = $this->positionRepository->delete($id, $type);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to delete data');
        }

        DB::commit();
        return $position;
    }

    public function updatePosition($data, $id, $type) {
        $validator = Validator::make($data, [
            'position' => 'required|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        DB::beginTransaction();

        try {
            $position = $this->positionRepository->update($data, $id, $type);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update data');
        }

        DB::commit();
        return $position;
    }
}
