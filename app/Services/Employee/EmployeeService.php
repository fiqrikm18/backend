<?php


namespace App\Services\Employee;


use App\Repositories\Employee\EmployeeRespository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EmployeeService
{
    protected $employeeRepository;

    public function __construct(EmployeeRespository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function getAllEmployee() {
        return $this->employeeRepository->findAll();
    }

    public function getEmployeeById($id) {
        return $this->employeeRepository->findById($id);
    }

    public function createEmployeeData($data) {
        $validator = Validator::make($data, [
            'nip' => 'required|string|max:10',
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }

        $employee = $this->employeeRepository->save($data);
        return $employee;
    }

    public function updateEmployeeData($data, $id) {
        $validator = Validator::make($data, [
            'nip' => 'required|string|max:10',
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|string'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }
        DB::beginTransaction();

        try {
            $employee = $this->employeeRepository->update($data, $id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to update data');
        }

        DB::commit();
        return $employee;
    }

    public function deleteEmployeeData($id) {
        DB::beginTransaction();

        try {
            $employee = $this->employeeRepository->delete($id);
        } catch (\Exception $e) {
            throw new InvalidArgumentException('Unable to delete data');
        }

        DB::commit();
        return $employee;
    }
}
